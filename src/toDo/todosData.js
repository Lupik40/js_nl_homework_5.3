const todosData = [
    {
        id: 1,
        text: "Почистить зубы",
        completed: true,
    },
    {
        id: 2,
        text: "Посмотреть урок WayUp",
        completed: true,
    },
    {
        id: 3,
        text: "Сделать домашку WayUp",
        completed: false,
    },
    {
        id: 4,
        text: "Дописать приложение",
        completed: false,
    }
];

export default todosData;
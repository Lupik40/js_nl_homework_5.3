import React from 'react';
import classes from './ToDoItem.module.scss';   

const ToDoItem = props => {
    return (
        <div className={classes.item}>
            <input type="checkbox" defaultChecked={props.completed}/>
            <p className={classes.name}>{props.description}</p>
        </div>
    )
}

export default ToDoItem;